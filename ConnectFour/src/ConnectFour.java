import java.util.Scanner;


public class ConnectFour {
	
	final static char RED='R';
	final static char YELLOW = 'Y';
	
	final static char EMPTY = ' ';
	final static int MAX_COLS= 7;
	final static int MAX_ROWS = 6;
	
	static String player;
	private static char[][] grid;
	
	
	public static void main(String[]args)
	{
		init();
		
		
		Scanner input = new Scanner(System.in);
		
		while(!isWin())
		{
			display();
			int location;
			do {
				int columnCount = MAX_COLS-1;
				System.out.print("Drop a "+ player + " disk at column (0-" + columnCount + ") :");
				location = input.nextInt();
			}while (location<0 || location>MAX_COLS-1);
			
			int row= MAX_ROWS-1;
			while (grid[location][row]!=EMPTY && row>=0)
			{
				row-=1;
				
				if (row<0) { break; }
			}
			
			if (player=="red" && row>=0) {  grid[location][row]=RED;
													player="yellow" ;}
			else if (player=="yellow" && row>=0 ){ grid[location][row]= YELLOW; 
													player="red";}
			else if (row<0)
			{
				System.out.println("Column location is full. Try your turn again.");
			}
		}	
		
	}
	
	public static boolean isWin()
	{
		int col = 0;
		int row = MAX_ROWS-1;
		int red =0;
		int yellow =0;
		
		// Check if 4 in a row
		do
		{
			if (grid[col][row]==RED)
			{
				red+=1;
				yellow=0;
			}
			
			if (grid[col][row]==YELLOW)
			{
				yellow+=1;
				red=0;
			}
			
			row=-1;
		} while ( yellow<4 && red<4  && row>=0);

		
		if (yellow==4)
		{
			System.out.println("The yellow player won.");
			return true;
		}
		
		else if (red==4)
		{
			System.out.println("The red player won.");
			return true;
		}
		
		else 
		{
			return false;
		}	
		
	}
	public ConnectFour()
	{
		
	}
	
	public static void init()
	{
		grid = new char[MAX_COLS][MAX_ROWS];
		
		for (int col=0; col<MAX_COLS; col++)
		{
			for (int row=0; row<MAX_ROWS; row++)
			{
				grid[col][row]=EMPTY;
			}
		}
		
		player= "red";
	}
	
	public static void display()
	{
		for (int row=0; row<MAX_ROWS; row++)
		{
			for (int col=0; col<MAX_COLS; col++)
			{
				System.out.print("|"+ grid[col][row]);
			}
			System.out.println("|"); // Go to the next row
		}
		for (int col=0; col<MAX_COLS; col++)
		{
			System.out.print("--");
		}
		System.out.println("-");
	}
}
